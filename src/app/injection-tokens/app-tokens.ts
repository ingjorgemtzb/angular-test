import { InjectionToken } from '@angular/core';

export const TITLE = new InjectionToken<string>('title');