import { Component, Inject, InjectionToken } from '@angular/core';
import { ProcessService } from './simple/process.service';
import { ProcessServiceFactory} from './factory/process.service'
import { PrintProcessService } from './interface/print-process-service';
import { TITLE } from './injection-tokens/app-tokens';
import { EnvService } from './global-enviroment/env.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    {provide: PrintProcessService, useExisting: ProcessServiceFactory}
  ]
  
})
export class AppComponent {
  private title = "Awesome process!";
  private output: String;

  constructor(
    private process: PrintProcessService,
    private env: EnvService
    ){
      console.log(env);
  }

  getTitle(): String {
    return this.title;
  }

  run(){
    this.output =  this.process.excecute();
  }

  getOutput(): String{
    return this.output;
  }
}
