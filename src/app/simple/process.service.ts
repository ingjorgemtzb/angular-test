import { Injectable } from '@angular/core';
import { AwesomeProcessService } from './awesome-process.service';
import { VeryAwesomeProcessService } from './very-awesome-process.service';
import { PrintProcessService } from '../interface/print-process-service';

@Injectable({
  providedIn: 'root',
  useClass: ProcessService
})
export class ProcessService extends PrintProcessService {

  constructor() {
    super();
    console.log('ProcessService');
  }

  excecute() : String {
    return '...../""""""""|======[]\n' +
    '..../""""""""""""|\n' +
    '/"""""""""""""""""""""""""\\\n' +
    '\\(@) (@) (@) (@) (@) (@)/';
  }
}
