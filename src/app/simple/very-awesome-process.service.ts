import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VeryAwesomeProcessService {

  constructor() { 
    console.log('VeryAwesomeProcessService');
  }

  excecute() : String {
    return '███۞███████ ]▄▄▄▄▄▄▄▄▄▄▄▄▃\n' +
    '▂▄▅█████████▅▄▃▂\n' +
    'I███████████████████].\n' +
    '◥⊙▲⊙▲⊙▲⊙▲⊙▲⊙▲⊙◤...';
  }
}
