import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AwesomeProcessService {

  constructor() {
      console.log('AwesomeProcessService');
   }

  excecute() : String {
    return '......(\\_/)\n' +
    '......( \'_\')\n' +
    '..../""""""""""""\======░ ▒▓▓█D\n' +
    '/"""""""""""""""""""\\\n'+
    '\\_@_@_@_@_@_/';
  }
}
