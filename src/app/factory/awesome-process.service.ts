import { Injectable } from '@angular/core';
import { AwesomeTank } from '../model/awesome-tank';
import { Tank } from '../interface/tank';

@Injectable({
  providedIn: 'root'
})
export class AwesomeProcessService {

  private tank: Tank;

  constructor() { 
    console.log('AwesomeProcessService');
    this.tank = new AwesomeTank();
  }

  excecute() : String {
    return this.tank.build();
  }
}
