import { Injectable } from '@angular/core';
import { AwesomeTank } from '../model/awesome-tank';
import { VeryAwesomeTank } from '../model/very-awesome-tank';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { Tank } from '../interface/tank';
import { SimpleTank } from '../model/simple-tank';

@Injectable({
  providedIn: 'root'
})
export class VeryAwesomeProcessService {

  private tank: Tank;
  
  constructor() { 
    console.log('VeryAwesomeProcessService');
    this.tank = new VeryAwesomeTank();
  }

  excecute() : String {
    return this.tank.build();
  }
}
