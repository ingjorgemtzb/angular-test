import { Injectable } from '@angular/core';
import { AwesomeProcessService } from './awesome-process.service';
import { VeryAwesomeProcessService } from './very-awesome-process.service';
import { ConfigurationService } from '../database/configuration.service';
import { TankWar } from '../model/tank-war';
import { PrintProcessService } from '../interface/print-process-service';
import { Tank } from '../interface/tank';


export const cultureFactory = (config: ConfigurationService) => {
  if (config.getConfiguration() == 1) {
    return new ProcessServiceFactory();
  } else if (config.getConfiguration() == 2) {
    return new AwesomeProcessService();
  }else if (config.getConfiguration() == 3) {
    return new VeryAwesomeProcessService();
  }else {
    return new ProcessServiceFactory();
  }
};

@Injectable({
  providedIn: 'root',
  useFactory: cultureFactory ,
  deps: [ConfigurationService]
})
export class ProcessServiceFactory extends PrintProcessService {

  private tank: Tank;

  constructor() {
    super();
    console.log('ProcessServiceFactory');
    this.tank = new TankWar();
   }

  excecute() : String {
    return this.tank.build();
  }
}
