import { TankWar } from './tank-war';

export class AwesomeTank extends TankWar{
    constructor(){
        super();
        this.header = '......(\\_/)\n' + '......( \'_\')\n';
        this.body = '..../""""""""""""\======░ ▒▓▓█D\n' +
        '/"""""""""""""""""""\\\n';
        this.wheels = '\\_@_@_@_@_@_/';
    }
}