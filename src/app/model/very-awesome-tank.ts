import { TankWar } from './tank-war';

export class VeryAwesomeTank extends TankWar {

    constructor(){
        super();
        this.header = '███۞███████ ]▄▄▄▄▄▄▄▄▄▄▄▄▃\n';
        this.body = '▂▄▅█████████▅▄▃▂\n' + 'I███████████████████].\n';
        this.wheels = '◥⊙▲⊙▲⊙▲⊙▲⊙▲⊙▲⊙◤...';
    }
}