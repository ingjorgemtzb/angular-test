import { Tank } from '../interface/tank';

export class SimpleTank implements Tank {
    build() {
        return '´!      /-----\\============@\n' +
       '´|_____/_______\_____\n'+
       '/____________________\\\n'+
        '\\+__+__+__+__+__+__*/';
    }
    
}