import { Tank } from '../interface/tank';

export class TankWar implements Tank{
    header: string;
    body: string;
    wheels: string;

    constructor(){
      this.header = '...../""""""""|======[]\n';
      this.body = '..../""""""""""""|\n' + '/"""""""""""""""""""""""""\\\n';
      this.wheels =  '\\(@) (@) (@) (@) (@) (@)/';
    }

    public build(): string{
        return this.header + this.body + this.wheels;
    }
}