import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {
  //default value
  public apiUrl = 'http://localhost:8080';

  public purchaseVersion = '2.4.0';
  
  constructor() { }
}
