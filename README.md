# Angular 9 #

Example of independency injection

### Dependency injection in Angular ###

Dependency injection (DI), is an important application design pattern. Angular has its own DI framework, which is typically used in the design of Angular applications to increase their efficiency and modularity.

Dependencies are services or objects that a class needs to perform its function. DI is a coding pattern in which a class asks for dependencies from external sources rather than creating them itself.

In Angular, the DI framework provides declared dependencies to a class when that class is instantiated.

### How inject dependecies in Angular? ###

* useClass
* useFactory
* useValue
* useExisting